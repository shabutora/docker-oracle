# Oracle 11g R2 Dockerfile

## Download Oracle Installer


## Usage

#### 1. Build docker container.

```
docker build .
```
#### 2. DB Install

By command line.

```
docker run -i -t -v download:/tmp/ora [container_id] /bin/bash
```

```
# su - oracle
$ ./db_install.sh
```

After Oracle install finished.  
Setup oracle listener.

```
$ netca -silent -responseFile /home/oracle/netca.rsp
```

#### 3. Running container

```
docker run 
```
