# Oracle Container

FROM centos:centos6

MAINTAINER Tsukasa Tamaru <tsukasa.tamaru@2dfacto.co.jp>

ENV http_proxy http://172.17.192.16:8080/
ENV https_proxy http://172.17.192.16:8080/

# Install Oracle Dependencies
WORKDIR /etc/yum.repos.d
RUN yum install wget unzip -y
RUN yum install libXp libXp-devel openmotif openmotif-devel compat-db -y
RUN wget https://public-yum.oracle.com/public-yum-ol6.repo --no-check-certificate
RUN wget https://public-yum.oracle.com/RPM-GPG-KEY-oracle-ol6 -O /etc/pki/rpm-gpg/RPM-GPG-KEY-oracle --no-check-certificate
RUN yum install oracle-rdbms-server-11gR2-preinstall -y

# Oracle User Settings
RUN echo "* - nproc 16384" >> /etc/security/limits.d/90-nproc.conf
USER oracle
ADD .bash_profile /home/oracle/.bash_profile
USER root
RUN mkdir -p /opt/oracle/product/11.2.0/dbhome_1
RUN chown -R oracle:oinstall /opt
RUN chmod -R 775 /opt

# Copy files
ADD files /home/oracle/install
RUN chown -R oracle:oinstall /home/oracle

EXPOSE 1521

